class MovieStorage {
    key = 'movieStorage';

    constructor() {
    localStorage.clear();
    }

    add(name, description, image, releaseYear, rating) {
        if (!this.validation(name, description, image, releaseYear, rating)) {
            return;
        }

        let newMovie = {
            name: name,
            description: description,
            image: image,
            releaseYear: releaseYear,
            rating: rating,
            date: new Date()
        }

        let currentList = this.getAll();
        currentList.push(newMovie);

        localStorage.setItem(this.key, JSON.stringify(currentList));
    }

    getAll() {
        const fromStorage = localStorage.getItem(this.key);

        return fromStorage === null ? [] : JSON.parse(fromStorage);
    }

    sort(key) {
        let currentList = this.getAll();

        let sorted = currentList.sort((a, b) => (a[key] > b[key]) ? 1 : -1);

        localStorage.setItem(this.key, JSON.stringify(sorted));
    }

    validation(name, description, image, releaseYear, rating) {
        if (typeof name !== 'string') {
            alert('The "name" is not a string');
            return false;
        }

        if (typeof description !== 'string') {
            alert('The "description" is not a string');
            return false;
        }

        if (typeof image !== 'string') {
            alert('The "image" is not a string');
            return false;
        }

        if (typeof releaseYear !== 'number' || !releaseYear) {
            alert('The "release year" is not a number');
            return false;
        }

        if (typeof rating !== 'string' ) {
            alert('The "rating" is not a string');
            return false;
        }

        return true;
    }
}

class ODBAPI {
    URL = 'https://www.omdbapi.com/?apikey=';
    key = 'fced3ba8&t';

    getMovie(title, callback) {
        fetch(this.URL + this.key + '&t=' + title)
            .then(response => response.json())
            .then(data => callback(data));
    }
}

class Application {
    backgroundColorIndex = 0;

    constructor() {
        this.MS = new MovieStorage();
        this.ODBAPI = new ODBAPI();

        // We populate with movies only if the movieStorage is empty
        if (!this.MS.getAll().length) {
            this.initMovies();
        }

        this.updateTable();
        this.initEvents();
    }

    initMovies() {
        this.MS.add(
            'Tokyo Story',
            'An old couple visit their children and grandchildren in the city, but receive little attention.',
            'https://m.media-amazon.com/images/I/91DUuBRoLnL._AC_UY327_FMwebp_QL65_.jpg',
            1953,
            '8.2/10'
        );

        this.MS.add(
            'The Dark Knight',
            'When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, Batman must accept one of the greatest psychological and physical tests of his ability to fight injustice.',
            'https://m.media-amazon.com/images/I/91ebheNmoUL._AC_UY327_FMwebp_QL65_.jpg',
            2008,
            '9.0/10'
        );

        this.MS.add(
            'Goodfellas',
            'The story of Henry Hill and his life in the mob, covering his relationship with his wife Karen Hill and his mob partners Jimmy Conway and Tommy DeVito in the Italian-American crime syndicate.',
            'https://m.media-amazon.com/images/I/81xpkHlK4bL._AC_UY327_FMwebp_QL65_.jpg',
            1990,
            '8.7/10'
        );
    }

    initEvents() {
        const self = this;
        document.getElementById('formSubmit').addEventListener('click', function () {
            self.formSubmit();
        });
        document.getElementById('changeColor').addEventListener('click', function () {
            self.changeColor();
        });
        document.getElementById('formSubmitAPI').addEventListener('click', function () {
            self.formSubmitAPI();
        });
    }

    sortTable(key) {
        this.MS.sort(key);
        this.updateTable();
    }

    updateTable() {
        this.clearTable();

        const movies = this.MS.getAll();

        movies.forEach(movie => {
            const date = new Date(movie.date);
            let html = `
            <tr>
                <td>${movie.name}</td>
                <td>${movie.description}</td>
                <td><img class="moviesImg" src="${movie.image}" alt="movie_image"></td>
                <td>${movie.releaseYear}</td>
                <td>${movie.rating}</td>
                <td>${date.toLocaleString()}</td>
            </tr>
            `;

            document.getElementById('moviesTable').innerHTML += html;
        });
    }

    clearTable() {
        const tableRows = document.querySelectorAll('#moviesTable tr');

        // We start from 1 because the row on index 0 is the header of the table
        for (let i = 1; i < tableRows.length; i++) {
            tableRows[i].remove();
        }
    }

    formSubmit() {
        const form = document.getElementById('movieForm');
        const formData = new FormData(form);

        this.MS.add(
            formData.get('name'),
            formData.get('description'),
            formData.get('image'),
            Number(formData.get('releaseYear')),
            formData.get('rating')
        );

        this.updateTable();
    }

    formSubmitAPI() {
        const form = document.getElementById('movieFormAPI');
        const formData = new FormData(form);

        this.ODBAPI.getMovie(
            formData.get('nameAPI'),
            function (movieData){
                if(movieData.Response && movieData.Response === 'False'){
                    alert(movieData.Error);
                    return;
                }
                App.MS.add(
                    movieData.Title,
                    movieData.Plot,
                    movieData.Poster,
                    Number(movieData.Year),
                    movieData.Ratings[0] ? movieData.Ratings[0].Value : '',
                );
                App.updateTable();
            }
        )
    }

    changeColor() {
        const colors = ['Lightpink', 'PaleVioletRed ', 'HotPink ', 'Orchid '];

        document.querySelector('body').style.backgroundColor = colors[this.backgroundColorIndex];

        this.backgroundColorIndex += 1;

        if (this.backgroundColorIndex >= colors.length) {
            this.backgroundColorIndex = 0;
        }
    }
}

window.onload = function () {
    window.App = new Application();
}
